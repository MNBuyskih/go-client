$ -> new Catalog()

class Catalog

    constructor: ->
        @bindEvents()
        @initSlider()

    bindEvents: ->
        $('[data-toggle="filter"]').on "click", (e) =>
            e.preventDefault()
            @toggleFilter()

        $this = @
        $('a', '#sorting').click (e) ->
            e.preventDefault()
            $this.loadCatalogItems(undefined, @) if !$(@).hasClass 'active'

        $('#large-filter-form').on 'submit', (e) =>
            e.preventDefault()
            @loadCatalogItems $(e.target)

        $('#lgFilterReset').on 'click', (e) =>
            e.preventDefault()
            form = $('#large-filter-form')
            $('input[type="text"], select', form).val ''
            $('input[type="checkbox"], input[type="radio"]', form).attr 'checked', false
            @loadCatalogItems form

    initSlider: ->
        $("#price-range").ionRangeSlider
            type: "double"
            grid: true
            grid_num: 5
            min: 0
            max: 10000
            min_interval: 1000
            onChange: (data) => @checkSliderLabels data.from, data.to
            onStart: (data) => @checkSliderLabels data.from, data.to

    toggleFilter: ->
        $('#small-filter').slideToggle()
        $('#large-filter').slideToggle()
        $('.filter').toggleClass 'large-filter'

    checkSliderLabels: (from, to) ->
        offset = 500
        $('.irs-grid-text').each (n, text) ->
            text = $(text)
            value = +(text.html().replace " ", '')
            text.toggle !(from > (value - offset) && from < (value + offset)) && !(to > (value - offset) && to < (value + offset))

    loadCatalogItems: (form, sorting) ->
        if !form
            form = if $('.filter').hasClass('large-filter') then $('#large-filter-form') else $('#small-filter-form')

        if !sorting
            sorting = $ 'a.active', '#sorting'

        sorting = $(sorting).data 'sort'
        location.href = location.pathname + "?#{form.serialize()}&sorting=#{sorting}"