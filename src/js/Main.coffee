$ ->
    $('body').addClass 'fixed-height'
    $('.main-slider').mainSlider()

    lastCoords = ""
    mouseMove = false
    startTimeout = ->
        clearTimeout mouseMove
        mouseMove = setTimeout ->
            $('body').addClass 'full-screen'
        , 5000
    startTimeout()
    $(document).on 'mousemove', (e) ->
        coords = "#{e.screenX}_#{e.screenY}"
        if lastCoords != coords
            $('body').removeClass 'full-screen'
            startTimeout()
        lastCoords = coords