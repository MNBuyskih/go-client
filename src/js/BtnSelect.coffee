class BtnSelect

    constructor: (@el) ->
        @el = $ @el
        @el.hide()

        options = @el.find "option"
        template = $ @template

        for option, index in options
            template.find("ul").eq(0).append "<li><a href='#' tabindex='-1' data-option-index='#{index}'>#{$(option).html()}</a></li>"
        template.insertAfter @el

        @dropdown = template

        @el.on 'change', => @activeLabel()

        $(@dropdown).on "click", "a", (e) =>
            e.preventDefault()
            index = $(e.target).data('option-index')
            val = @el.val()
            @el.find('option').attr 'selected', false
            @el.find('option').eq(index).attr 'selected', true
            @el.trigger 'change' if val isnt @el.val()

        @activeLabel()

    activeLabel: ->
        option = @el.find('option:selected')
        val = option.val()

        if !val && label = @el.attr('data-select-title')
        else if option.length == 1
            label = option.html()
            selected = !!val
        else
            label = "&nbsp;"

        btn = @dropdown.find('.btn')
        btn.find('span').eq(1).html label

        if selected
            btn.removeClass('btn-default').addClass('btn-success')
        else
            btn.removeClass('btn-success').addClass('btn-default')

    template: '<div class="dropdown">
    <div data-toggle="dropdown" class="btn btn-default btn-block dropdown-toggle"><span class="caret"></span><span></span></div>
    <ul class="dropdown-menu"></ul>
</div>'

$.fn.btnSelect = ->
    for el in @
        new BtnSelect(el) if el.tagName == "SELECT"

$ ->
    $('.btn-select').btnSelect()