$ -> new Accessories()

class Accessories

    constructor: ->
        inside = $ '.filter-carusel-inside'
        inputs = $ '.filter-carusel-item', inside
        width = inputs.length * 100 + inputs.length * 20
        inside.css('width', "#{width}px")

        $('.arrow', '.filter-carusel').click (e) ->
            e.preventDefault()
            cont = $('.filter-carusel-scroll')
            scLeft = cont.scrollLeft()
            dir = if $(@).hasClass 'arrow-left' then 1 else -1
            cont.animate
                scrollLeft: scLeft + (120 * dir)

        $this = @
        $('a', '#sorting').click (e) ->
            e.preventDefault()
            $this.setSorting(@) if !$(@).hasClass 'active'

    setSorting: (sorting) ->
        sorting = $(sorting).data 'sort'
        query = location.search

        if /[?&]sort=/.test query
            query = query.replace /([?&]sort=)[^&]+/, "$1#{sorting}"
        else
            if /^\?/.test query
                query = "#{query}&sort=#{sorting}"
            else
                query = "?sort=#{sorting}"

        location.href = location.pathname + query