class MainSlider

    constructor: (@el) ->
        @el = $ @el
        @items = @el.find(".main-slider-item")

        if @items.length
            @injectList()
        @items.each (n, item) =>
            $(item).css 'z-index', @items.length - n
            @injectListItem item
            @setImage item
        @injectListArrows()

        onAnimate = false

        @el.on "mousewheel", (e) =>
            if !onAnimate
                if e.deltaY > 0
                    @prev()
                else if e.deltaY < 0
                    @next()

                onAnimate = true
                setTimeout ->
                    onAnimate = false
                , 500

        @pager.find('.main-slider-pager-arrow-up').on 'click', (e) =>
            e.preventDefault()
            @prev()

        @pager.find('.main-slider-pager-arrow-down').on 'click', (e) =>
            e.preventDefault()
            @next()

        $this = @
        as = @pager.find('a')
        as.on 'click', (e) ->
            e.preventDefault()
            index = $.inArray @, as
            if index > -1
                $this.current = index
                $this.slide()

        @slide()

    current: 0

    slide: ->
        #$(@items).removeClass 'active'
        $(@items[@current])
        .removeClass 'slideTop'
        .prevAll ".main-slider-item"
        .addClass "slideTop"

        li = @pager.find('li')[1..]
        $(li).removeClass 'active'
        $(li[@current]).addClass 'active'

    prev: ->
        @current-- if @current > 0
        @slide()

    next: ->
        @current++ if @current < (@items.length - 1)
        @slide()

    setImage: (item) ->
        $item = $(item)
        src = $item.find('img').attr "src"
        $item.css "background-image", "url(#{src})"

    injectList: ->
        @el.append "<ul class='main-slider-pager'></ul>"
        @pager = @el.find('.main-slider-pager')

    injectListItem: ->
        @pager.append "<li><a href='#'></a></li>"

    injectListArrows: ->
        @pager.prepend "<li class='main-slider-pager-arrow-up'></li>"
        @pager.append "<li class='main-slider-pager-arrow-down'></li>"

        setTimeout =>
            height = @pager.height() / 2
            @pager.css "margin-top", "-#{height}px"
        , 100

jQuery.fn.mainSlider = ->
    for el in @
        new MainSlider el