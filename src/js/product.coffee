$ -> new Product()

class Product

    constructor: ->
        $this = @
        $('.product-images-arrow').click (e) ->
            e.preventDefault()
            $this.slide(if $(@).hasClass('product-images-arrow-up') then -1 else 1)

        @pItems = $('.product-images-item')
        @pItems.click (e) =>
            e.preventDefault()
            @setActiveImage e.target

        @setActiveImage @pItems.first()

    setActiveImage: (thumb) ->
        thumb = $ thumb
        @pItems.removeClass 'active'
        thumb.addClass 'active'

        image = thumb.data('image')
        $('.product-image-image').css 'background-image', "url(#{image})"

    slide: (direction) ->
        cont = $('.product-images-inner')
        startScroll = cont.scrollTop()
        cont.animate
            scrollTop: startScroll + 83 * direction