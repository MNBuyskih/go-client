$ ->
    $('.navbar-toggle').click (e) ->
        e.preventDefault()
        $('body').toggleClass 'nav-visible'

    $('.nav-search-btn').click (e) ->
        e.preventDefault()

        body = $ 'body'
        className = 'search-visible'
        if !body.hasClass className
            body.addClass className
            setTimeout ->
                $('.nav-search-input input').focus()
            , 200
        else
            # search submit

    $('.nav-search-close').click (e) ->
        e.preventDefault()
        $('body').removeClass 'search-visible'

    $('.m-main-slider .swiper-slide').each (n, el) ->
        $el = $ el
        img = $el.find('img').attr 'src'
        $el.css 'background-image': "url(#{img})"

    if Swiper?
        $ '.swiper-container'
        .each (n, container) ->
            new Swiper container,
                direction: 'horizontal'
                loop: false
                pagination: $ '.swiper-pagination', container
                nextButton: $ '.swiper-button-next', container
                prevButton: $ '.swiper-button-prev', container

    new SelectBtn '#filter'

class SelectBtn

    template: '<div class="select-btn">Показать:&nbsp;<span></span></div>'

    constructor: (@select) ->
        @select = $ @select
        @html = $ @template
        @select.before @html

        @select.css
            opacity: 0
            position: 'absolute'
            top: @html.position().top
            left: @html.position().left
            width: @html.outerWidth()
            height: @html.outerHeight()

        @select.change => @setLabel()
        @setLabel()

    setLabel: ->
        option = $('option:selected', @select)
        label = option.html()
        @html.find 'span'
        .html label