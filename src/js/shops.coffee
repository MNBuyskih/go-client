$ -> new Shops()

class Shops

    constructor: ->
        date = new Date()
        _d = date.getTime()

        $ '.shops-time'
        .each (_n, el) ->
            el = $ el
            i = el.find 'i'

            fromH = el.data 'fromh'
            fromM = el.data 'fromm'
            toH = el.data 'toh'
            toM = el.data 'tom'

            d = new Date()
            d.setHours toH, toM
            d = d.getTime()

            if d < _d
                # Магазин закрыт. красный свет
                i.addClass 'text-danger'
            else if d - _d < 60 * 60 * 1000
                # Если до закрытия осталось меньше часа
                i.addClass 'text-warning'

            zeroPad = (m, _n = +m) -> if _n < 10 then "0#{_n}" else _n

            el.find('span').html "#{zeroPad(fromH)}:#{zeroPad(fromM)}-#{zeroPad(toH)}:#{zeroPad(toM)}"
