module.exports = (grunt) ->

    grunt.initConfig
        watch:
            jade:
                files: "src/**/*.jade"
                tasks: "jade"
            coffee:
                files: "src/**/*.coffee"
                tasks: "newer:coffee"
            less:
                files: "src/**/*.less"
                tasks: "less"
            copy:
                files: ["src/css/images/*", "src/images/*"]
                tasks: "copy"

        coffee:
            compile:
                options:
                    bare: false
                expand: true
                cwd: 'src/js'
                src: '**/*.coffee'
                dest: 'dest/js'
                ext: '.js'

        jade:
            compile:
                files: [
                    'dest/accessories.html': 'src/accessories.jade'
                    'dest/company.html': 'src/company.jade'
                    'dest/Main.html': 'src/Main.jade'
                    'dest/phones.html': 'src/phones.jade'
                    'dest/tablets.html': 'src/tablets.jade'
                    'dest/product.html': 'src/product.jade'
                    'dest/shops.html': 'src/shops.jade'
                    'dest/guarantee.html': 'src/guarantee.jade'
                    'dest/news.html': 'src/news.jade'

                # Мобильная версия
                    'dest/mobile/main.html': 'src/mobile/main.jade'
                    'dest/mobile/phones.html': 'src/mobile/phones.jade'
                    'dest/mobile/product.html': 'src/mobile/product.jade'
                    'dest/mobile/news.html': 'src/mobile/news.jade'
                    'dest/mobile/guarantee.html': 'src/mobile/guarantee.jade'
                    'dest/mobile/company.html': 'src/mobile/company.jade'
                    'dest/mobile/contacts.html': 'src/mobile/contacts.jade'
                ]
                options:
                    pretty: true
                    data:
                        catalogItems: grunt.file.readJSON("dumy/phones.json")
                        shops: grunt.file.readJSON("dumy/shops.json")

        less:
            compile:
                options:
                    compress: true
                files: [
                    "dest/css/style.css": "src/css/style.less"
                    "dest/css/mobile.css": "src/css/mobile.less"
                ]

        copy:
            copy:
                files: [
                    # images
                    {
                        expand: true
                        cwd: "src/css/images"
                        src: ["*"]
                        dest: "dest/css/images"
                    }
                    {
                        expand: true
                        cwd: "src/images"
                        src: ["*"]
                        dest: "dest/images"
                    }

                    #fonts
                    {
                        expand: true
                        cwd: "src/css/fonts/"
                        src: ["*"]
                        dest: "dest/css/fonts"
                    }

                    {
                        "dest/js/jquery.js": "bower_components/jquery/dist/jquery.min.js"
                        "dest/js/jquery.min.map": "bower_components/jquery/dist/jquery.min.map"
                        "dest/js/jquery.mousewheel.js": "bower_components/jquery-mousewheel/jquery.mousewheel.min.js"

                        "dest/js/bootstrap.js": "bower_components/bootstrap/dist/js/bootstrap.min.js"

                        "dest/js/ion.rangeSlider.js": "bower_components/ionrangeslider/js/ion.rangeSlider.min.js"

                        "dest/js/swiper.min.js": "bower_components/Swiper/dist/js/swiper.jquery.min.js"
                        "dest/js/swiper.js": "bower_components/Swiper/dist/js/swiper.jquery.js"
                        "dest/js/maps/swiper.jquery.min.js.map": "bower_components/Swiper/dist/js/maps/swiper.jquery.min.js.map"
                    }
                ]

        clean:
            clean: "dest"

    grunt.loadNpmTasks 'grunt-contrib-watch'
    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.loadNpmTasks 'grunt-contrib-jade'
    grunt.loadNpmTasks 'grunt-contrib-copy'
    grunt.loadNpmTasks 'grunt-contrib-less'
    grunt.loadNpmTasks 'grunt-contrib-clean'
    grunt.loadNpmTasks 'grunt-newer'

    grunt.registerTask 'default', [
        'clean'
        'copy'
        'coffee'
        'jade'
        'less'
    ]